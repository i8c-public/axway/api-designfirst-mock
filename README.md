# Design and mock your APIs using Stoplight and Axway API Builder

# Introduction

Nowadays multiple customers adopted the usage of APIs and a microservice architecture as their integration strategy. What makes an API management initiative successful is the usage of the APIs within the organization. On top of the non-functional requirements like security, performance, scalability, etc. one important aspect is how to define a meaningful definition of an API and how to give it to the consumer's audience in no-time for further refinement. 
Following the best practice for designing a REST API is often not enough, as the this is usually achieved by an iterative approach in collaboration with the consumer's audience. 
What is really important is to share the API as fast as possible with the different consumers and further refine it to cope with their needs. This is possible to achieve with a minimum involvement of the development team. This process is also perfectly aligned in a DevOps context where the first stage could be a Sandbox that can be given to your customer at early stage. Moreover, a CI/CD pipeline could be use to promote the API assets to the different stages, but this time no more with a mock server behind.

# Context
The main point to address is to have a useful API at early stage, so that can be called by the different consumer to mimic the real behavior. We don't want to have only static responses, but something more realistic. We also want to have a useful mocked backend at early stage, but without involving the development team as we want to keep the implementation efforts low at this stage.
On the market multiple alternatives exist, but I will mention only some of them like SmartBear [ReadyAPI](https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwiWwdyApcvuAhWJuHcKHfR2BZIYABAAGgJlZg&ohost=www.google.com&cid=CAESQeD253sLGWcjMJ_TQ5cZtlLGmgSZfyxMX7MaWG_CLS6Cw5b1eG8I6YzZfWKvSlNKyzWbZkK6kt5s9zkJWY1u5vX-&sig=AOD64_1ldIOidsRTjCjr2pF8m_jKGrhDbA&q&adurl&ved=2ahUKEwivntWApcvuAhXP0aQKHcSbCUUQ0Qx6BAgZEAE), [Postman](https://www.postman.com) or even some custom implementation in any language that comes with an Object Relational Mapper (Java, C#, NodeJs). In this blog, I will explore the combination of [Stoplight](https://stoplight.io/) with Axway API Builder and I will showcase the process from the definition a Swagger specification of an API together with its mock service. 

# Configure Stoplight with your Git Repository

The first step is to have Stoplight in place. This will be the main entry of you API assets repository. Stoplight will not only be a design tool, but it will be linked to an internal Git repository so all the API designers can collaborate on the definition of the API specifications together with the creation of the common JSON objects to be reused within the organization. Once you registered your account and your organization in Stoplight.io, then you need to link a Git repository to the newly created workspace. In this way all the API specifications will be available between all the developers and they can collaborate on. 

![](./images/AddGit.PNG)   

With Stoplight you can easily apply a Design First approach, ad you can define your JSON Metamodel and link it to your API resources. In this example I created a simple [BookStore](resources/BookStore.json) API that offers the capability to create new items and search for the Book resources. The API itself is only an example of CRUD functionality that can be exposed towards your consumer. 
Stoplight offers also a nice feature that allows to check your API specifications against a set of predefined rules. Stoplight Studio has a direct integration with [Spectral](https://stoplight.io/open-source/spectral/) linter, so the company design guidelines can be easily enforced at the time of the design of your API. It is also possible to enforce those rules in a CI/CD pipeline, so each swagger definition can be checked against the company rules and not promoted in case of errors. In the picture below I am showing the log of a CI/CD Gitlab Pipeline where all the API swagger definition under the resources folder are validated.

![](./images/spectralPipeline.PNG)  

Spectral has a set of built-in functions to help you build custom rules for your JSON objects. These functions include pattern checks, parameter checks and many other features. You can also create a set of custom ruleset, like for instance the one I defined that is checking for the presence of a http endpoint or for the style pattern for writing the names of the tags. This rule is generating an error if it founds a http definition under the schemas array or if the name of the tags are not camelCased.
In the picture below the CI/CD pipeline failed as the swagger file was allowing http communication (only https is enforced by the rule) and the name of the tags were not camelcased.

![](./images/SpectralFailingRule.PNG)

Unfortunately having only the API specification in place is not enough, we need to make it callable and it should mimic the behavior of the production one. For this purpose I will set up also as a backend service: a mock implementation using Axway API Builder. Stoplight offers also a mocking functionality embedded, but it is much more limited than the features offered by Axway API Builder. 

![](./images/BookStore.PNG)  

# Installing Axway API Builder

API Builder is a powerful tool that allows you to build and deploy multiple API resources that can be consumed by any client application. An API Builder project is a Node.js application and is made by several components that you can use in your project flow. You can either define the components using JavaScript files placed in specific directories, which are automatically loaded when creating an API Builder instance or programmatically create components after initializing an API Builder instance. Let's now install it locally. For this purpose I used a CentOS Linux distribution.

- Install the AMPLIFY CLI globally using npm. 

```sh
[sudo] npm install -g @axway/amplify-cli
```

- Install API Builder CLI 

```sh
amplify pm install @axway/amplify-api-builder-cli
```

- Once the installation is complete then you can create a new project 

```sh
amplify builder init myproject
```

- Finally run the project

```sh
    cd myproject
    npm start
```

If everything worked fine you should be able to access the API builder by hitting the ``http://localhost:8080/console`` url.

![](./images/APIBuilder.PNG)  

API Builder doesn't need to run locally,  you can also package it as a docker image and run it in a container orchestration platform like Openshift. For more information see [this](https://devblog.axway.com/apis/api-builder-microservice-2/).

# Mocking the BookStore API

You can now import your API in API Builder. Mocking the API is super easy, as the only thing that is needed is to load your API definition by fetching an url or loading it from your filesystem. After that by simply clicking on the Save and mock button, your API mock will be already up and running.

![](./images/ImportAndMock.PNG)  

By doing this, a set of mocking flows are automatically generated by Axway API Builder by reading the Swagger specification of the API. In the [BookStore](resources/BookStore.json) API, some examples where defined for the defined objects. For instance, a Book entity was defined and this is what it will be returned back by the mocking service

![](./images/APIResposeExample.PNG) 

As visible in the picture below, API Builder set up a mocking flow that is returning the values that were defined in the Swagger file

![](./images/APIBuilderMockingFlow.PNG) 

Clearly, this is really a basic demonstration of the capability of the tool, but it can quickly create a dynamic user experience by applying the set of filters available in the Flow-Triggers Workbench. Let's for instance generate different reply based on the book id that is passed as input parameter. What I will show is how to generate different authors and book names and return an id value according to the one specified in the API request. The overall flow is the one shown below:

![](./images/GetBookFlow.PNG)

The flow is quite simple and depending of the id passed as input of the get books/{id} resource, it returns different values for the author and the book name. Also, the id passed as input in the url, is also returned back accordingly. I defined an array of 4 items using the Javascript plugin, but those values could be easily retrieved by an external data source like a mysql database or many other data sources by using the different plugins offered by Axway API Builder. Here there is a code snippet where this simple logic is implemented.

![](./images/Javascript.PNG)

In the images below I show the different results of the API call based on the different input ids.

![](./images/Req1.PNG)

![](./images/Req2.PNG)

![](./images/Req3.PNG)







# Instructions

The resource folder contains the Swagger definition of the BookStore API and the Axway API Builder bookstore project that has been used to showcase the mocking flow. The ruleset folder instead contains the custom spectral rule that is checking the swagger files under the resources folder. This is triggered by the Gitlab CI/CD pipeline in the project.
var APIBuilder = require('@axway/api-builder-runtime');
var Model = APIBuilder.createModel('Error', {
    "connector": "memory",
    "fields": {
        "code": {
            "type": "string",
            "required": true
        },
        "message": {
            "type": "string",
            "required": true
        },
        "description": {
            "type": "string"
        }
    },
    "actions": []
});
module.exports = Model;